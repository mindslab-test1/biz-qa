package maum.biz.qa;

import maum.biz.qa.common.utils.seed.SeedCryptUtil;

import java.text.SimpleDateFormat;
import java.util.Date;


public class FubonSeedTest {
	public static void main(String[] args) throws Exception {
		String seedkey = "#sTt45tA!Sim8pLq";
		String lastLoginedTime = new SimpleDateFormat("MMddHHmm",java.util.Locale.KOREAN).format(new Date());
		String paramVal = "86859"+"_"+lastLoginedTime; //86859 사번
		String encParamVal = SeedCryptUtil.encryptDataHex(paramVal, seedkey);
		System.out.println("Original Text      :: [" + paramVal + "]");
		System.out.println("Encoding Text      :: [" + encParamVal + "]");

		String decParamVal = SeedCryptUtil.decryptDataHex(encParamVal, seedkey);

		System.out.println("Decoding Text      :: [" + decParamVal + "]");

		// 사번 날짜시간구하기
		int idx = decParamVal.indexOf("_");
		String empno = decParamVal.substring(0,idx);
		String strDate = decParamVal.substring(idx+1);

		System.out.println("empno        :: [" + empno + "]");
		System.out.println("strDate      :: [" + strDate + "]");

		// 로그인유지시간 체크하기 30분

	}
}
