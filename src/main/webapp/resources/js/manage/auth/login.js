$(document).ready(function(){
    putSaveId();  // ID 저장정보 삽 입

    // 아이디, 비밀번호 입력여부 확인하여 아이콘 활성화
    $('.login_form > input').on('keydown', function(){
        $(this).addClass('on');
    });
    $('.login_form > input').on('keyup', function(){
        if( $(this).val() === ''){
            $(this).removeClass('on');
        }
    });
});

//initialize. 아이디저장 체크에 따른 초기값 세팅
function putSaveId(){
    var ckVal = $.cookie('saveId');

    if( $('#saveId').is(":checked") && ckVal.length > 0 ){
        $('#user_id').val(ckVal);
        $('#user_pw').focus();
    }else{
        $('#user_id').focus();
    }
}

//Login
function goLogin(){
    var id = $('#user_id').val();
    var pw = $('#user_pw').val();

    if( id.length < 1 ){
        alert("ID를 입력하시기 바랍니다.");
        $('#user_id').val();
        $('#user_id').focus();
        return false;
    }
    else if( pw.length < 1 ){
        alert("비밀번호를 입력하시기 바랍니다.");
        $('#user_pw').val();
        $('#user_pw').focus();
        return false;
    }

    if( $('#save_id').is(":checked") ){
        $.cookie('save_id', id, {expire:7});
    }

    document.login_form.method = "POST";
    document.login_form.action = "/loginProcess";
    document.login_form.submit();
}

//엔터키 처리
function enterCheck(event){
    if( event.keyCode == 13 ){
        goLogin();
    }else{
        return false;
    }
}