function sample(){
    $.ajax({
        type: 'POST',
        url: '/script/sample',
        data: {data1: "data1"},
        beforeSend : function(xhr) {
            // 데이터를 전송하기 전에 헤더에 csrf값을 설정한다
            xhr.setRequestHeader(_csrf.headerName, _csrf.token);
            xhr.setRequestHeader("AJAX", "true");
        },
        error: function(result){
            if (result.status == 302){
                alert("로그인 세션이 만료되었습니다 \n새로고침 후 다시 시도해주세요.");
                location.href = "/login";
            } else {
                alert("시스템 에러 \n(새로고침 후 다시시도 해주세요)");
                console.error(result);
            }
        },
        success: function(result){
            alert(result);
        }
    });

}