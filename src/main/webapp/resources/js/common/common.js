﻿$(document).ready(function () {
    menu_layout();  // 메뉴 레이아웃
});

// 메뉴 레이아웃
function menu_layout(){
    // gnb
    $(".gnb_menu > li").on("click", function () {
        if ($(this).hasClass("active")) {
            $(this).removeClass("active");
            $(".gnb_sub_menu").show();
        } else {
            $(".gnb_menu > li").removeClass("active");
            $(this).addClass("active");
            $(".gnb_sub_menu").hide();
        }
    });

    $(".gnb_menu > li").on("mouseover", function () {
        $(this).addClass("active");
        $(".gnb_sub_menu").show();
    });

    $(".gnb_menu > li").on("mouseleave", function () {
        $(this).removeClass("active");
        $(".gnb_sub_menu").hide();
    });

    // ios hover 오류 대응
    $("a").on("click touchend", function () {
        if ($(this).attr('target') === '_blank') {
            window.open = $(this).attr("href");
        } else {
            window.location = $(this).attr("href");
        }
    });
}

// 팝업 오픈
function fn_Pop(id, cb=function () {}) {
    // 부모 창 스크롤 막기
    $("body").css("overflow", "hidden");

    // 팝업 배경 띄우기
    $(".pop_bg").show();

    // 팝업 띄우기
    // 팝업 타겟은 팝업 호출 function에 팝업 고유 pop_ + 뒤에 오는 id 값 설정
    // ex = id = pop_map -> fn_pop("map");
    $("#pop_" + id).show();

    let marginTopNum = 2;
    let marginBottomNum = 0;
    if (id == 'scriptDetail'){
        marginTopNum = 1.55;
        marginBottomNum = 10;
    }

    // 팝업 가운데 띄우기
    $(window).resize(function () {
        var $modal = $('.popup,.pop_wrap');

        $modal_visible = $('.popup:visible');
        $modal_visible.css({
            marginTop: - $modal_visible.height() / marginTopNum,
            marginLeft: - $modal_visible.width() / 2,
            marginBottom: $modal_visible.height() / marginBottomNum,
        });
    }).resize();

    if (typeof cb == 'function') {
        cb();
    }
}

// 팝업 클로즈
function fn_Pop_Close() {
    // 부모 창 스크롤 가능하게
    $("body").css("overflow", "auto");

    // 팝업, 팝업 배경 닫기
    $(".pop_wrap, .pop_bg").hide();
}

// form data to json
$.fn.serializeObject = function(){
    var obj = null;
    try {
        if(this[0].tagName && this[0].tagName.toUpperCase() == "FORM" ) {
            var arr = this.serializeArray();
            if(arr){ obj = {};
                jQuery.each(arr, function() {
                    obj[this.name] = this.value;
                });
            }
        }
    }
    catch(e){
        alert(e.message);
    }
    finally {

    }

    return obj;
};

// JSON null or undefined to empty string
function nullToEmptyString(data){
    data = JSON.stringify(data, function (key, value) {return (value === null) ? "" : value});
    return JSON.parse(data);
}

// 문자타입 숫자의 남은 자리를 0으로 채우기
function pad(n, width) {
    n = n + '';
    return n.length >= width ? n : new Array(width - n.length + 1).join('0') + n;
}


