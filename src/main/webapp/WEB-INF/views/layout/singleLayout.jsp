<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- css -->
    <link rel="stylesheet" href="/resources/css/common.css">
    <link rel="stylesheet" href="/resources/css/login.css">

    <!-- js -->
    <script src="/resources/js/libs/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/libs/jquery-migrate-3.3.0.min.js"></script>
    <script src="/resources/js/libs/jquery.cookie.js"></script>

    <!--[if lt IE 9]>
    <script src="/resources/js/utils/html5.js"></script>
    <![endif]-->
</head>
<body>
    <tiles:insertAttribute name="container"/>
</body>
</html>