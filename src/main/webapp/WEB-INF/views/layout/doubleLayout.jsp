<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- css -->
    <!-- datetimepicker -->
    <link rel="stylesheet" href="/resources/css/bootstrap.css"/>
    <link rel="stylesheet" href="/resources/css/bootstrap-datetimepicker.css"/>
    <!-- datatables -->
    <link rel="stylesheet" href="/resources/css/datatables.min.css">
    <link rel="stylesheet" href="/resources/css/buttons.dataTables.min.css">

    <link rel="stylesheet" href="/resources/css/common.css">
    <link rel="stylesheet" href="/resources/css/stt_tm_qa.css">

    <!-- js -->
    <script src="/resources/js/libs/jquery-3.3.1.min.js"></script>
    <script src="/resources/js/libs/jquery-migrate-3.3.0.min.js"></script>
    <!-- datetimepicker -->
    <script src="/resources/js/libs/bootstrap-datetimepicker.js"></script>
    <script src="/resources/js/libs/moment.min.js"></script>
    <script src="/resources/js/libs/ko.js"></script>
    <!-- datatables -->
    <script src="/resources/js/libs/datatables.min.js"></script>
    <script src="/resources/js/libs/dataTables.fixedColumns.min.js"></script>
    <script src="/resources/js/libs/dataTables.buttons.min.js"></script>
    <script src="/resources/js/libs/buttons.html5.min.js"></script>
    <script src="/resources/js/libs/jszip.min.js"></script>
    <!-- jsrender -->
    <script src="/resources/js/libs/jsrender.min.js"></script>
    <script src="/resources/js/common/common.js"></script>

    <!--[if lt IE 9]>
    <script src="/resources/js/utils/html5.js"></script>
    <![endif]-->




</head>
<body>
    <div id="wrap">
        <tiles:insertAttribute name="header"/>
        <tiles:insertAttribute name="container"/>
    </div>
</body>
</html>