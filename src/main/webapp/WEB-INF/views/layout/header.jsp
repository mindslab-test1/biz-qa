<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- #header -->
<div id="header">
    <!-- .title_box -->
    <div class="title_box bg_blue">
        <h1>
            <img src="resources/images/img_logo_w.png" class="logo" alt="푸본현대생명 로고">
            <p class="title">STT TM_QA</p>
        </h1>

        <div class="etc_box">
            <!-- [D] 사용자 계정 표시 영역(.user_account span) / 임의로 '사용자계정 표시'라고 텍스트 넣어둠 -->
            <div class="user_account">
                <span>사용자계정 표시</span>
            </div>
            <div class="btnBox btn_logout">
                <a href="#none">
                    <img src="resources/images/ico_logout_w.png" alt="로그아웃 아이콘">
                    로그아웃
                </a>
            </div>
        </div>
    </div>
    <!-- //.title_box -->

    <!-- .gnb -->
    <!-- [D] 현재 페이지에 해당하는 메뉴(.gnb li)에 addclass('current')주면 활성화됨 -->
    <!-- [D] 대메뉴(.gnb > li) 마우스오버 시 중메뉴(.sub) slidedown되며 보여짐 -->
    <!-- [D] 소메뉴(.third)가 존재하는 중메뉴(.snb > li)는 오른쪽에 아래방향 화살표가 추가되며, 클릭 시 소메뉴(.third) slidedown되며 보여짐 -->
    <ul class="gnb">
        <li>
            <h3>Home</h3>

            <ul class="sub">
                <li>
                    <a href="#none">Dashboard</a>
                </li>
            </ul>
        </li>
        <li class="current">
            <h3>CS</h3>

            <ul class="sub">
                <li class="current">
                    <a href="#none">CS - STT 대상</a>
                </li>
            </ul>
        </li>
        <li>
            <h3>TM</h3>

            <ul class="sub">
                <li>
                    <a href="#none">TM - STT 대상</a>
                </li>
            </ul>
        </li>
        <li>
            <h3>TM_QA</h3>

            <ul class="sub">
                <li>
                    <a href="#none">QA 배정</a>
                </li>
                <li>
                    <a href="#none">QA 심사리스트</a>
                </li>
                <li>
                    <a href="#none">QA 결과</a>
                </li>
            </ul>
        </li>
        <li>
            <h3>통계</h3>

            <ul class="sub">
                <li>
                    <a href="#none">상담원 통계</a>
                </li>
                <li>
                    <a href="#none">심사자 통계</a>
                </li>
            </ul>
        </li>
        <li>
            <h3>모니터링</h3>

            <ul class="sub">
                <li>
                    <a href="#none">처리 상태 모니터링</a>
                </li>
                <li>
                    <a href="#none">서비스 모니터링</a>
                </li>
            </ul>
        </li>
        <li>
            <h3>관리설정</h3>

            <ul class="sub">
                <li>
                    <a href="#none">표준 스크립트 관리</a>
                    <ul class="third">
                        <li>
                            <a href="#none">상품 스크립트 관리</a>
                        </li>
                        <li>
                            <a href="#none">점검 항목 관리</a>
                        </li>
                        <li>
                            <a href="#none">표준 스크립트 관리</a>
                        </li>
                        <li>
                            <a href="#none">금지어&sol;유의어 관리</a>
                        </li>
                        <li>
                            <a href="#none">문장 발화 조건</a>
                        </li>
                        <li>
                            <a href="#none">답변 관리</a>
                        </li>
                        <li>
                            <a href="#none">문장 관리</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#none">시뮬레이션 화면</a>
                </li>
                <li>
                    <a href="#none">사용자 관리</a>
                    <ul class="third">
                        <li>
                            <a href="#none">사용자 관리</a>
                        </li>
                        <li>
                            <a href="#none">권한 관리</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#none">재처리 관리</a>
                    <ul class="third">
                        <li>
                            <a href="#none">TA 재처리</a>
                        </li>
                        <li>
                            <a href="#none">STT&sol;TA 재처리</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
    </ul>
    <!-- //.gnb -->
</div>
<!-- //#header -->


<!-- script -->
<script type="text/javascript">
    $(document).ready(function(){
        // [공통] gnb 메뉴 script
        function gnbShowSlide() {
            // 대메뉴 마우스오버 시 중메뉴 슬라이드 다운으로 보여짐
            $('.gnb > li').hover(function(){
                $(this).find('.sub').stop().slideToggle(200);
                $(this).toggleClass('active');
                $('.third').slideUp();
            });

            // 소메뉴가 존재하는 중메뉴에 화살표 추가됨
            if($('.third').length){
                $('.third').parents('li').addClass('down');
            }

            // 중메뉴 클릭 시 소메뉴 슬라이드 다운으로 보여짐
            $('.sub a').on('click', function(e){
                e.stopPropagation();
                $(this).parents('.sub').css('height', 'auto');
                $(this).next().slideToggle(200);
                $(this).removeClass('down');
            });
        }
        gnbShowSlide();
    });
</script>

