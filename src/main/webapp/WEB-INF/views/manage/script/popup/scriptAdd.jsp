<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!-- pop_wrap의 id 값으로 팝업 호출 / id 변경하시면 호출 funtion도 변경하셔야 합니다. -->
<!-- 관련 소스는 common.js 참고 -->
<!-- 문장 추가 팝업 -->
<div class="pop_wrap" id="pop_scriptAdd">
    <div class="popup" style="width:800px;">
        <div class="pop_header">
            <h1>문장 추가</h1>
            <button type="button" class="pop_close" onclick="closeSaPop()">팝업창 닫기</button>
        </div>
        <div class="pop_body">
            <div class="board">
                <table>
                    <colgroup>
                        <col style="width:140px" />
                        <col style="width:auto" />
                        <col style="width:100px" />
                    </colgroup>
                    <tbody>
                    <tr>
                        <th scope="col">검색 문장 입력</th>
                        <td><input type="text" id='saPopSearchSentenceTxt' class="lg" onkeypress="enterCheck('scriptAdd')"/></td>
                        <td><button type="button" class="btn btn_blue" onclick="saPopGetSentenceInfo()">검색</button></td>
                    </tr>
                    </tbody>
                </table>
            </div>

            <div class="board">
                <table id="saPopSentenceTb">
                    <colgroup>
                        <col style="width:100px" />
                        <col style="width:auto" />
                        <col style="width:100px" />
                    </colgroup>
                    <thead>
                    <tr>
                        <th scope="col">NO</th>
                        <th scope="col">표준스크립트 문장내용</th>
                        <th scope="col">선택</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td colspan="3">검색한 문장이 없습니다.</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="pop_bg"></div> <!-- 팝업 배경 -->
</div>
<!-- // 문장 추가 팝업 -->

<script src="/resources/js/manage/script/popup/scriptAdd.js" type="text/babel" data-presets="es2015, stage-2"></script>
