<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!-- .login_wrap -->
<div id="wrap" class="login_wrap">
    <!-- .login_area -->
    <div class="login_area">
        <!-- .img_box -->
        <div class="img_box fl">
            <div class="img_group">
                <h1>푸본현대생명 STT TM_QA</h1>
                <img class="logo_img" src="resources/images/img_logo_color.png" alt="푸본현대생명 로고">
                <img class="bg_img" src="resources/images/bg_login.png" alt="로그인화면 이미지">
            </div>
        </div>
        <!-- //.img_box -->

        <!-- .login_box -->
        <div class="login_box fr">
            <div class="login_group">
                <h2>Login</h2>

                <form id="login_form" name="login_form">
                    <fieldset class="login_form">
                        <legend>로그인</legend>
                        
                        <label for="user_id" class="hide">아이디</label>
                        <input type="text" id="user_id" name="userId" placeholder="아이디" value="admin" onkeypress="enterCheck(event)">
                        
                        <label for="user_pw" class="hide">비밀번호</label>
                        <input type="password" id="user_pw" name="password" placeholder="비밀번호" value="1234" onkeypress="enterCheck(event)">
    
                        <div class="chkBox">
                            <input type="checkbox" id="save_id">
                            <label for="save_id">아이디 저장</label>
                        </div>
    
                        <!-- [D] 아이디, 비밀번호 입력 오류 시 등장하는 안내 문구 / addclass('on')주면 보여짐 -->
                        <span class="login_error on">아이디 또는 비밀번호를 다시 확인해주세요.</span>
                        
                        <div class="btnBox">
                            <a href="#none" class="bg_blue" onclick="goLogin()">로그인</a>
                        </div>
                    </fieldset>
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                </form>
            </div>                            
        </div>
        <!-- //.login_box -->
    </div>
    <!-- //.login_area -->
</div>
<!-- //.login_wrap -->

<!-- script -->
<script src="/resources/js/manage/auth/login.js"></script>
