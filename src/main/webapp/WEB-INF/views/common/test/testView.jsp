<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form id="testForm" name="testForm">
    <input type="hidden" id="function_name" name="function_name" value="getTestList" />
    <input type="hidden" id="current_page_no" name="current_page_no" value="1" />
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<!-- #container -->
<div id="container">
    <!-- #contents -->
    <div id="contents">
        <h4>Test &hyphen; TestView</h4>


    </div>
    <!-- //#contents -->
</div>
<!-- //#container -->

<!-- script -->
<script type="text/javascript">
    var userId= "${fn:escapeXml(sessionScope.accessUser.userId)}";

    $(document).ready(function(){

        alert(userId);
        getTestList();
    });

    /** CsStt - 목록 조회  */
    function getTestList(){

        $.ajax({
            url		:"/test/list",
            data    : $("#testForm").serialize(),
            dataType:"JSON",
            cache   : false,
            async   : true,
            type	:"POST",
            success : function(obj) {
                console.log(obj);
                //$("#apiAccountList").html($("#apiList").render(obj));
                // $("#pagination").html(obj.data.pagination);
            },
            error 	: function(xhr, status, error) {}

        });
    }
</script>