<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<form id="csSttForm" name="csSttForm">
    <input type="hidden" id="function_name" name="function_name" value="getCsSttList" />
    <input type="hidden" id="current_page_no" name="current_page_no" value="1" />
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>
<!-- #container -->
<div id="container">
    <!-- #contents -->
    <div id="contents">
        <h4>CS &hyphen; STT</h4>

        <!-- .srchArea -->
        <div class="srchArea">
            <table class="tbl_line_view" summary="상담일시, 고객 전화번호, 고객번호, 무음길이, 사번, 업무구분, 상담유형(대), 상담유형(중), 상담유형(소)으로 구성">
                <caption class="hide">검색조건</caption>
                <colgroup>
                    <col width="100"><col width="310"><col width="100"><col><col width="100"><col><col width="100"><col><col width="100"><col>
                </colgroup>

                <tbody>
                    <tr>
                        <th>상담일시</th>
                        <!-- [D] 처음 페이지 들어왔을때의 초기 상담일시는 [전날 날짜, 오전 9시] - [당일 날짜, 미정] 기본 입력 상태여야함 -->
                        <!-- [D] 시간 초기 기본값 미정으로 비워둠 -->
                        <td>
                            <div class="iptBox">
                                <input type="text" id="fromDate" class="iptDateTime">
                                <span>&hyphen;</span>
                                <input type="text" id="toDate" class="iptDateTime">
                            </div>
                        </td>

                        <th>고객 전화번호</th>
                        <td>
                            <div class="iptBox">
                                <input type="text" numberOnly>
                            </div>
                        </td>

                        <th>고객번호</th>
                        <td>
                            <div class="iptBox">
                                <input type="text" numberOnly>
                            </div>
                        </td>

                        <th>증권번호</th>
                        <td>
                            <div class="iptBox">
                                <input type="text" numberOnly>
                            </div>
                        </td>

                        <th>사번</th>
                        <td>
                            <div class="iptBox">
                                <input type="text" numberOnly>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <th>업무구분</th>
                        <td>
                            <!-- [D] 선택박스 옵션 10개까지 보여짐, 그 외 스크롤로 확인가능 -->
                            <div class="sltBox custom">
                                <select class="select">
                                    <option value="">전체</option>
                                    <option value="">X-SELL</option>
                                    <option value="">기타 아웃바운드 안내</option>
                                    <option value="">내 보험 찾아줌</option>
                                    <option value="">당월 미납안내</option>
                                    <option value="">당월 실효부활안내 해피콜</option>
                                    <option value="">등기 반송안내</option>
                                    <option value="">만족도조사</option>
                                    <option value="">반송안내</option>
                                    <option value="">사전콜</option>
                                    <option value="">생존&sol;만기</option>
                                    <option value="">미지급안내</option>
                                    <option value="">신계약 해피콜</option>
                                    <option value="">실효예고안내</option>
                                    <option value="">업무문의</option>
                                    <option value="">연금도래안내</option>
                                    <option value="">연금미지급 미신청</option>
                                    <option value="">연락처 유입오류안내</option>
                                    <option value="">예약호통화</option>
                                </select>
                            </div>
                        </td>

                        <!-- [D] 상담유형(대) 선택해야 상담유형(중) 활성화됨 -->
                        <th>상담유형&lpar;대&rpar;</th>
                        <td>
                            <div class="sltBox custom largeType">
                                <select class="select">
                                    <option value="">전체</option>
                                    <option value="">개인</option>
                                    <option value="">법인</option>
                                    <option value="">변액</option>
                                    <option value="">VIP</option>
                                    <option value="">BA</option>
                                </select>
                            </div>
                        </td>

                        <!-- [D] 상담유형(중) 선택해야 상담유형(소) 활성화됨 -->
                        <!-- [D] 선택박스(.sltBox)에 addclass('disabled')하면 비활성화 효과 적용됨 -->
                        <th>상담유형&lpar;중&rpar;</th>
                        <td>
                            <div class="sltBox custom mediumType disabled">
                                <select class="select">
                                    <option value="">전체</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                </select>
                            </div>
                        </td>

                        <!-- [D] 선택박스(.sltBox)에 addclass('disabled')하면 비활성화 효과 적용됨 -->
                        <th>상담유형&lpar;소&rpar;</th>
                        <td>
                            <div class="sltBox custom smallType disabled">
                                <select class="select">
                                    <option value="">전체</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                    <option value="">상담유형옵션</option>
                                </select>
                            </div>
                        </td>

                        <th>무음길이</th>
                        <td>
                            <div class="iptBox">
                                <input type="text" numberOnly>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>

            <!-- [D] 조회 버튼(.btn_srch)클릭 시 하단 리스트 오름차순으로 기본 정렬된 상태로 조회되며, 입력된 검색조건 값들은 reset되어야함 -->
            <!-- [D] 초기화 버튼(.btn_reset)클릭 시 검색조건 테이블 상 입력값들 모두 reset됨 -->
            <div class="btnBox">
                <a href="#none" class="btn_srch">조회</a>
                <a href="#none" class="btn_reset">초기화</a>
            </div>
        </div>
        <!-- //.srchArea -->

        <!-- .dataTableBox -->
        <div class="dataTableBox">
            <!-- [D] datatables 라이브러리 영역 -->
            <!-- [D] 'Excel 다운로드' 버튼 클릭 시 파일 다운로드됨 -->
            <!-- [D] 처음 페이지 들어왔을때 목록 순서는 '상담일시'기준으로 오름차순 정렬됨 -->
            <!-- [D] 실제 테이블데이터로 해피콜, 완전판매여부가 포함되어있지만 브라우저 상에서는 hidden 처리 되어있으며 엑셀 다운로드 하면 함께 노출됨 -->
            <table class="dataTable">
                <thead>
                    <tr>
                        <th>NO</th>
                        <th>상담일시</th>
                        <th>고객 전화번호</th>
                        <th>상담고객</th>
                        <th>고객번호</th>
                        <th>증권번호</th>
                        <th>상담사</th>
                        <th>사번</th>
                        <th>상담경로</th>
                        <th>업무구분</th>
                        <th>상담유형&lpar;대&rpar;</th>
                        <th>상담유형&lpar;중&rpar;</th>
                        <th>상담유형&lpar;소&rpar;</th>
                        <th>상담키워드</th>
                        <th>무음길이</th>
                        <th>녹취번호</th>
                        <th>상담번호</th>
                        <th>STT 상태</th>
                        <!-- [D] 실제 테이블데이터로 해피콜, 완전판매여부가 포함되어있지만 브라우저 상에서는 hidden 처리 되어있으며 엑셀 다운로드 하면 함께 노출됨 -->
                        <th>해피콜</th>
                        <th>완전판매여부</th>
                    </tr>
                </thead>

                <!-- [D] 조회된 목록(tbody tr) 더블클릭 시 CS STT 전수 팝업 윈도우 팝업으로 등장 -->
                <!-- [D] 4번째 상담고객 이름 두번째문자 마스킹 해야함 -->
                <!-- [D] 13번째 상담키워드는 최대 16글자로 모두 노출되어야함 -->
                <tbody>
                    <tr>
                        <td>1</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>홍길동</td>
                        <td>100917026</td>
                        <td>7777777</td>
                        <td>김영희</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>내보험찾아줌</td>
                        <td>개인</td>
                        <td>보험계약대출</td>
                        <td>청약철회접수-가족반대</td>
                        <td>보험계약대출신청보험계약대출신청대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>2020-06-15 10:20</td>
                        <td>010-1234-5678</td>
                        <td>홍길옥</td>
                        <td>3643883</td>
                        <td>7777777</td>
                        <td>김철수</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>보험계약대출</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>자유납입해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>2020-06-15 10:19</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>이아라</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>업무문의</td>
                        <td>변액</td>
                        <td>보험계약대출</td>
                        <td>해지사유_무응답</td>
                        <td>추가납입문의, 추가납입문의</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>정태민</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>VIP</td>
                        <td>보험계약대출</td>
                        <td>해지사유_상품불만</td>
                        <td>자동대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영미</td>
                        <td>2026112</td>
                        <td>7777777</td>
                        <td>권현범</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>BA</td>
                        <td>보험계약대출</td>
                        <td>해지사유_타사상품중복</td>
                        <td>자동대출해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>김영재</td>
                        <td>103649784</td>
                        <td>7777777</td>
                        <td>김영</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>개인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>납입일시중지신청</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>김만이</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>가입문의</td>
                        <td>납입일시중지해지</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>민영애</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김아리</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>카드이체해지</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영옥</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김범</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>BA</td>
                        <td>가입문의</td>
                        <td>급여이체신청/변경</td>
                        <td>자동이체변경, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>106241799</td>
                        <td>7777777</td>
                        <td>이사라</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>개인</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>자동이체해지, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>장영진</td>
                        <td>3612944</td>
                        <td>7777777</td>
                        <td>김희</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>법인</td>
                        <td>가입문의</td>
                        <td>결제일변경</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>허영</td>
                        <td>3510429</td>
                        <td>7777777</td>
                        <td>홍길동</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>홍길동</td>
                        <td>100917026</td>
                        <td>7777777</td>
                        <td>김영희</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>내보험찾아줌</td>
                        <td>개인</td>
                        <td>보험계약대출</td>
                        <td>청약철회접수-가족반대</td>
                        <td>보험계약대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>홍길옥</td>
                        <td>3643883</td>
                        <td>7777777</td>
                        <td>김철수</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>보험계약대출</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>자유납입해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>이아라</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>업무문의</td>
                        <td>변액</td>
                        <td>보험계약대출</td>
                        <td>해지사유_무응답</td>
                        <td>추가납입문의, 추가납입문의</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>정태민</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>VIP</td>
                        <td>보험계약대출</td>
                        <td>해지사유_상품불만</td>
                        <td>자동대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영미</td>
                        <td>2026112</td>
                        <td>7777777</td>
                        <td>권현범</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>BA</td>
                        <td>보험계약대출</td>
                        <td>해지사유_타사상품중복</td>
                        <td>자동대출해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>김영재</td>
                        <td>103649784</td>
                        <td>7777777</td>
                        <td>김영</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>개인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>납입일시중지신청</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>김만이</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>가입문의</td>
                        <td>납입일시중지해지</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>20</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>민영애</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김아리</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>카드이체해지</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>21</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영옥</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김범</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>BA</td>
                        <td>가입문의</td>
                        <td>급여이체신청/변경</td>
                        <td>자동이체변경, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>22</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>106241799</td>
                        <td>7777777</td>
                        <td>이사라</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>개인</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>자동이체해지, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>23</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>장영진</td>
                        <td>3612944</td>
                        <td>7777777</td>
                        <td>김희</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>법인</td>
                        <td>가입문의</td>
                        <td>결제일변경</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>24</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>허영</td>
                        <td>3510429</td>
                        <td>7777777</td>
                        <td>홍길동</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>25</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>홍길동</td>
                        <td>100917026</td>
                        <td>7777777</td>
                        <td>김영희</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>내보험찾아줌</td>
                        <td>개인</td>
                        <td>보험계약대출</td>
                        <td>청약철회접수-가족반대</td>
                        <td>보험계약대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>26</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>홍길옥</td>
                        <td>3643883</td>
                        <td>7777777</td>
                        <td>김철수</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>보험계약대출</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>자유납입해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>27</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>이아라</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>업무문의</td>
                        <td>변액</td>
                        <td>보험계약대출</td>
                        <td>해지사유_무응답</td>
                        <td>추가납입문의, 추가납입문의</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>28</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>정태민</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>VIP</td>
                        <td>보험계약대출</td>
                        <td>해지사유_상품불만</td>
                        <td>자동대출신청</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>29</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영미</td>
                        <td>2026112</td>
                        <td>7777777</td>
                        <td>권현범</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>BA</td>
                        <td>보험계약대출</td>
                        <td>해지사유_타사상품중복</td>
                        <td>자동대출해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>30</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>김영재</td>
                        <td>103649784</td>
                        <td>7777777</td>
                        <td>김영</td>
                        <td>86397</td>
                        <td>TRS</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>개인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>해지사유_귀국/이민/해외근무</td>
                        <td>납입일시중지신청</td>
                        <td>6</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>31</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>이영희</td>
                        <td>107390420</td>
                        <td>7777777</td>
                        <td>김만이</td>
                        <td>86397</td>
                        <td>IN</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>법인</td>
                        <td>신계약/부활/청철/품보</td>
                        <td>가입문의</td>
                        <td>납입일시중지해지</td>
                        <td>3</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>정상</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>32</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>민영애</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김아리</td>
                        <td>86397</td>
                        <td>OUT</td>
                        <td>당월실효부활안내해피콜</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>카드이체해지</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>33</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>박영옥</td>
                        <td>1</td>
                        <td>7777777</td>
                        <td>김범</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>BA</td>
                        <td>가입문의</td>
                        <td>급여이체신청/변경</td>
                        <td>자동이체변경, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>34</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>법정상속인</td>
                        <td>106241799</td>
                        <td>7777777</td>
                        <td>이사라</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>개인</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>자동이체해지, 카드</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>35</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>장영진</td>
                        <td>3612944</td>
                        <td>7777777</td>
                        <td>김희</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>법인</td>
                        <td>가입문의</td>
                        <td>결제일변경</td>
                        <td>급여이체신청</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                    <tr>
                        <td>36</td>
                        <td>2020-06-15 10:23</td>
                        <td>010-1234-5678</td>
                        <td>허영</td>
                        <td>3510429</td>
                        <td>7777777</td>
                        <td>홍길동</td>
                        <td>86130</td>
                        <td>OUT</td>
                        <td>기타아웃바운드안내</td>
                        <td>변액</td>
                        <td>가입문의</td>
                        <td>가입문의</td>
                        <td>급여이체해지</td>
                        <td>5</td>
                        <td>007602f5338c69f6</td>
                        <td>149860675</td>
                        <td>오류</td>
                        <td>해피콜</td>
                        <td>완전판매여부</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //.dataTableBox -->
    </div>
    <!-- //#contents -->
</div>
<!-- //#container -->

<!-- script -->
<script type="text/javascript">
$(document).ready(function(){
    // select 스크롤 커스텀
    function customScrollSelectBox(){
        var SelectBoxScroll = $('.sltBox.custom select');
        SelectBoxScroll.each(function (i, select){
            if (!$(this).next().hasClass('slt_scroll')) {
                $(this).after('<div class="slt_scroll wide ' + ($(this).attr('class') || '') + '" tabindex="0"><span class="current_option"></span><div class="list"><ul></ul></div></div>');
                var dropdown = $(this).next();
                var options = $(select).find('option');
                var selected = $(this).find('option:selected');
                dropdown.find('.current_option').html(selected.data('display-text') || selected.text());
                options.each(function (j, o) {
                    var display = $(o).data('display-text') || '';
                    dropdown.find('ul').append('<li class="option ' + ($(o).is(':selected') ? 'selected' : '') + '" data-value="' + $(o).val() + '" data-display-text="' + display + '">' + $(o).text() + '</li>');
                });
            }
        });

        // Open/close
        $(document).on('click', '.slt_scroll', function(e){
            if($(e.target).hasClass('dd-searchbox')){
                return;
            }
            $('.slt_scroll').not($(this)).removeClass('open');
            $(this).toggleClass('open');
            if ($(this).hasClass('open')){
                $(this).find('.option').attr('tabindex', 0);
                $(this).find('.selected').focus();
            }else{
                $(this).find('.option').removeAttr('tabindex');
                $(this).focus();
            }
        });

        // select 박스 외 영역 클릭 시 닫기
        $(document).on('click', function(e){
            if ($(e.target).closest('.slt_scroll').length === 0) {
                $('.slt_scroll').removeClass('open');
                $('.slt_scroll .option').removeAttr('tabindex');
            }
            e.stopPropagation();
        });

        // Option 클릭 시 text 가져오기
        $(document).on('click', '.slt_scroll .option', function(e){
            $(this).closest('.list').find('.selected').removeClass('selected');
            $(this).addClass('selected');
            var text = $(this).data('display-text') || $(this).text();
            $(this).closest('.slt_scroll').find('.current_option').text(text);
            $(this).closest('.slt_scroll').prev('select').val($(this).data('value')).trigger('change');
        });

        // Keyboard events
        $(document).on('keydown', '.slt_scroll', function (e) {
            var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
            // Space or Enter
            //if (event.keyCode == 32 || event.keyCode == 13) {
            if (e.keyCode == 13) {
                if ($(this).hasClass('open')) {
                    focused_option.trigger('click');
                } else {
                    $(this).trigger('click');
                }
                return false;
                // Down
            } else if (e.keyCode == 40) {
                if (!$(this).hasClass('open')) {
                    $(this).trigger('click');
                } else {
                    focused_option.next().focus();
                }
                return false;
                // Up
            } else if (e.keyCode == 38) {
                if (!$(this).hasClass('open')) {
                    $(this).trigger('click');
                } else {
                    var focused_option = $($(this).find('.list .option:focus')[0] || $(this).find('.list .option.selected')[0]);
                    focused_option.prev().focus();
                }
                return false;
                // Esc
            } else if (e.keyCode == 27) {
                if ($(this).hasClass('open')) {
                    $(this).trigger('click');
                }
                return false;
            }
        });
    }
    customScrollSelectBox();

    // input:text[numberOnly] 숫자만 입력가능
    $('input:text[numberOnly]').keypress(function(event) {
        if(event.which < 48 || event.which > 57){
            event.preventDefault();
        }
    });

    // 초기화 버튼 클릭 시 검색조건 초기화
    $('.btn_reset').on('click', function(){
        $('.srchArea input').val('');
        $('.srchArea .slt_scroll .list .option').removeClass('selected');
        $('.srchArea .slt_scroll .list .option:first-child').addClass('selected');

        var optionSelected = $('.srchArea .slt_scroll .list .option.selected');
        optionSelected.each(function(){
            var optionText = $(this).text();
            $('.current_option').text(optionText);
        });
    });

    // datetimepicker 렌더링
    $('#fromDate').datetimepicker({
        language : 'ko', // 화면에 출력될 언어를 한국어로 설정합니다.
        pickTime : false, // 사용자로부터 시간 선택을 허용하려면 true를 설정하거나 pickTime 옵션을 생략합니다.
        defalutDate : new Date(), // 기본값으로 오늘 날짜를 입력합니다. 기본값을 해제하려면 defaultDate 옵션을 생략합니다.
        autoclose: 1, // 날짜 선택 시 달력 자동 꺼짐
    });

    $('#toDate').datetimepicker({
        language : 'ko', // 화면에 출력될 언어를 한국어로 설정합니다.
        pickTime : false, // 사용자로부터 시간 선택을 허용하려면 true를 설정하거나 pickTime 옵션을 생략합니다.
        defalutDate : new Date(), // 기본값으로 오늘 날짜를 입력합니다. 기본값을 해제하려면 defaultDate 옵션을 생략합니다.
        autoclose: 1, // 날짜 선택 시 달력 자동 꺼짐
    });

    // datatable 렌더링
    var table = $('.dataTable').DataTable({
        dom: 'iptlB', // 옵션 위치 지정
        language: { // 테이블 목록 건수 text 한글 설정
            emptyTable: "조회된 데이터가 없습니다.", // 데이터 없을 경우 노출되는 text 설정
            infoEmpty: "&lbrack;총 _TOTAL_ 건&nbsp;&nbsp; <strong>_END_</strong> 건 &sol; _TOTAL_ 건&rbrack;",
            info: "&lbrack;총 _TOTAL_ 건&nbsp;&nbsp; <strong>_END_</strong> 건 &sol; _TOTAL_ 건&rbrack;",
            lengthMenu: "_MENU_ 건씩 보기",
            paginate: {
                first: "",
                previous: "",
                next: "",
                last: ""
            },
        },
        responsive: true, // 반응형 허용
        filter: false, // 검색창 미허용
        order: [[ 1, 'asc' ]], // 초기 sort 설정
        paging: true, // 페이징 허용
        pagingType: "full_numbers", // 페이징 버튼 구성 (처음, 이전, 페이지숫자, 다음, 마지막)
        lengthMenu: [25, 50, 100], // 페이지 당 보여질 목록 갯수 select 범위 지정
        scrollY: true, // 세로 스크롤 허용, 사이즈
        scrollX: true, //가로 스크롤 허용
        scrollCollapse: true,
        columnDefs: [ // 컬럼별 width, 옵션 사용 지정
            {targets: [0], width: "50px", sortable: false},                    // NO
            {targets: [1], width: "150px"},                                    // 상담일시
            {targets: [2], width: "150px"},                                    // 고객 전화번호
            {targets: [3], width: "100px", orderable: false},                  // 상담고객
            {targets: [4], width: "120px"},                                    // 고객번호
            {targets: [5], width: "120px"},                                    // 증권번호
            {targets: [6], width: "100px", orderable: false},                  // 상담사
            {targets: [7], width: "100px"},                                    // 사번
            {targets: [8], width: "100px", orderable: false},                  // 상담경로
            {targets: [9], width: "150px"},                                    // 업무구분
            {targets: [10], width: "100px", orderable: false},                 // 상담유형(대)
            {targets: [11], width: "150px", orderable: false},                 // 상담유형(중)
            {targets: [12], width: "150px", orderable: false},                 // 상담유형(소)
            {targets: [13], width: "300px", orderable: false},                 // 상담 키워드
            {targets: [14], width: "100px"},                                   // 무음길이
            {targets: [15], width: "150px", orderable: false},                 // 녹취번호
            {targets: [16], width: "120px", orderable: false},                 // 상담번호
            {targets: [17], width: "100px"},                                   // STT 상태
            {targets: [18], width: "120px", orderable: false, visible: false}, // 해피콜
            {targets: [19], width: "120px", orderable: false, visible: false}, // 완전판매여부
        ],
        dom: 'Blfrtip', // 엑셀 다운로드 버튼 추가
        buttons: [{
            extend: 'excelHtml5',
            text: 'Excel 다운로드',
        }]
    });

    new $.fn.dataTable.FixedColumns(table, {
        fixedColumns: { // 왼쪽 1열 고정
            leftColumns: 1,
            heightMatch: 'none'
        }
    });

    // 다른 열에서 sort되어도 1열 순번은 그대로 유지
    table.on('order.dt search.dt', function(){
        table.column(0, {search:'applied', order:'applied'}).nodes().each(function (cell, i){
            cell.innerHTML = i+1;
        });
    }).draw();

    // datatable height 브라우저 height 기준 maxheight 조정
    function tableResize() {
        // 전체 테이블
        $('.DTFC_ScrollWrapper').css({
            height: "100%",
            maxHeight: ($(window).height() - 410) + 'px'
        });
        // 테이블 바디 (스크롤 포함)
        $('.DTFC_LeftBodyWrapper').css({
            maxHeight: ($(window).height() - 416 - 40) + 'px'
        });
        // 첫번째 열
        $('.dataTables_scrollBody').css({
            maxHeight: ($(window).height() - 410 - 40) + 'px'
        });
    }
    tableResize();
    $(window).resize(function() {
        tableResize();
    });
});
</script>
<script type="text/javascript">
    //var userId= "${fn:escapeXml(sessionScope.accessUser.userId)}";

    $(document).ready(function(){
        getCsSttList();
    });

    /** CsStt - 목록 조회  */
    function getCsSttList(){

        $.ajax({
            url		:"/cs/csStt/list",
            data    : $("#csSttForm").serialize(),
            dataType:"JSON",
            cache   : false,
            async   : true,
            type	:"POST",
            success : function(obj) {
                console.log(obj);
                //$("#apiAccountList").html($("#apiList").render(obj));
                // $("#pagination").html(obj.data.pagination);
            },
            error 	: function(xhr, status, error) {}

        });
    }
</script>