package maum.biz.qa.model.manage.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QaScriptInfo {
    private String stringData1;
    private String stringData2;
    private Integer integerData1;
    private Integer integerData2;

}
