package maum.biz.qa.model.manage.dto;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.manage.entity.QaScriptInfo;

@Getter
@Setter
public class ScriptDTO extends QaScriptInfo {

    private String stringData11;
    private String stringData12;
    private String stringData13;
    private String stringData14;


}
