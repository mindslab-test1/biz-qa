package maum.biz.qa.model.common.enumm;

import lombok.Getter;

@Getter
public enum MenuAuthRoleEnum {

    R01("조회"),
    R02("조회등록"),
    R03("조회수정"),
    R04("조회삭제"),
    R05("조회등록삭제"),
    R06("조회등록수정"),
    R07("조회수정삭제"),
    R08("조회등록수정삭제");

    private String codeNm;

    MenuAuthRoleEnum(String codeNm) {
        this.codeNm = codeNm;
    }
}
