package maum.biz.qa.model.common.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import maum.biz.qa.model.common.entity.CommonVo;

/**
 * 공통 DTO 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
@EqualsAndHashCode(callSuper=false)
public class CommonDto extends CommonVo {

	private Integer limit;
	private Integer offset;
	private String pagination;
}
