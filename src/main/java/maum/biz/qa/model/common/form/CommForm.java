package maum.biz.qa.model.common.form;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;


@Getter
@Setter
public class CommForm {

    // 페이징 관련
    private String pageInitPerPage;
    private String currentPage;
    private int startRow;
    private int lastRow;

    // 로그인된 ID
    private String creatorId = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();
    private String updaterId = ((UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername();


}
