package maum.biz.qa.model.common.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 화면에서 사용하는 관리사용자 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AdminUserForm extends CommonForm {

	private String userId;
	private String userNm;
	private String userPw;
	private Integer enabled;
	private String authority;
	private Integer autCd;

	private String search_type;
	private String search_keyword;

	private Integer adminUserIdArr[];
}
