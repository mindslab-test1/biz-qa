package maum.biz.qa.model.common.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.sql.Date;

@Getter
@Setter
public class EmpInfo {
    private Integer EMPNO;
    private String  ENAME;
    private String JOB;
    private Integer MGR;
    private Date HIREDATE;
    private Integer SAL;
    private Integer COMM;
    private Integer DEPTNO;
}