package maum.biz.qa.model.common.form;

import lombok.Getter;
import lombok.Setter;

/**
 * 화면에서 사용하는 공통 Form 모델
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class CommonForm {

	private String function_name;
	private Integer current_page_no;
	private Integer count_per_page;
	private Integer count_per_list;
	private Integer total_page_count;
	private Integer total_list_count;
	private Integer limit;
	private Integer offset;
	private String viewType;

	private String updatorId;
	private String updatedTm;
	private String creatorId;
	private String createdTm;
}
