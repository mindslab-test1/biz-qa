package maum.biz.qa.model.common.entity;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.entity.CommonVo;

/**
 * 관리사용자 DB 모델 (QA_USR_MGT_TB)
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
public class AdminUser extends CommonVo {
	private String userId;
	private String userNm;
	private String userPw;
	private String empNo;
	private String autCd;
	private Integer enabled;
	private String authority;
}