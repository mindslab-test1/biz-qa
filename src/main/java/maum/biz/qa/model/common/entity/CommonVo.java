package maum.biz.qa.model.common.entity;

import lombok.Data;

/**
 * 데이터베이스에서 사용하는 공통 모델
 *
 * @author unongko
 * @version 1.0
 */

@Data
public class CommonVo {
	private Integer num;
	private String updatorId;
	private String updatedTm;
	private String creatorId;
	private String createdTm;
}
