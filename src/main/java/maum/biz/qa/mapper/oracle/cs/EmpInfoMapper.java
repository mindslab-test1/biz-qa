package maum.biz.qa.mapper.oracle.cs;

import maum.biz.qa.model.common.entity.EmpInfo;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface EmpInfoMapper {

    EmpInfo getEmpInfoDetail(Integer empNo);
}
