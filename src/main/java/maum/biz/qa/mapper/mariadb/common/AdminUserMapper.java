package maum.biz.qa.mapper.mariadb.common;

import maum.biz.qa.model.common.dto.AdminUserDto;
import maum.biz.qa.model.common.form.AdminUserForm;
import maum.biz.qa.model.common.entity.UserPrincipal;
import maum.biz.qa.model.common.entity.AdminUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 매핑파일에 기재된 sql을 호출하기 위한 interface
 *
 * @author unongko
 * @version 1.0
 * @see /adminUser.xml
 */

@Repository
@Mapper
public interface AdminUserMapper {
    /** 시큐리티 로그인 정보 조회 */
    UserPrincipal findUserByLoginId(@Param("userId") String loginId);

    /** 로그인 id 조회 */
    String getLoginId(String decEmpno);

    /** 로그인 계정 상세 조회 */
    AdminUser getLoginAdminUserDetail(@Param("userId") String userId);

}