package maum.biz.qa.controller.manage;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class ManageViewController {

    // 스크립트 관리 페이지/
    @RequestMapping(value="/manage/script", method = RequestMethod.GET)
    public String goScript(){ return "manage/script/script"; }


}
