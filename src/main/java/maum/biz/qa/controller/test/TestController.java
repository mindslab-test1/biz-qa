package maum.biz.qa.controller.test;

import com.google.gson.Gson;
import maum.biz.qa.controller.common.HomeController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
public class TestController {
    private static final Logger logger = LoggerFactory.getLogger(TestController.class);

    @GetMapping("/test/main")
    public ModelAndView testMain(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("TestController.{}", "testMain()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "dashboardHome");
        httpSession.setAttribute("menuName", "DASHBOARD");
        httpSession.setAttribute("subMenuName", "");

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("common/test/testView");

        Gson gson = new Gson();

        logger.info("testMain() httpSession : {}", gson.toJson(httpSession));

        return modelAndView;
    }
}
