package maum.biz.qa.controller.test;

import maum.biz.qa.common.config.PropertyConfig;
import maum.biz.qa.model.common.entity.EmpInfo;
import maum.biz.qa.model.manage.entity.QaScriptInfo;
import maum.biz.qa.service.cs.EmpInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestApiController {

    @Autowired
    PropertyConfig propertyConfig;

    @Autowired
    EmpInfoService empInfoService;

    @Value("${env.seedkey}")
    private String seedkey;

    @RequestMapping("/test")
    public String test() {
        String prof =  System.getProperty("spring.profiles.active");
//        String seedkey =  System.getProperty("env.obj.seedkey");

        System.out.println("TestApiController seedkey !!! " + seedkey);

//        String returnStr = "Server : " + prof;

        String returnStr = propertyConfig.getObj().getNumber() + " / " + propertyConfig.getObj().getText() ;

        return returnStr;
    }

    // 샘플
    @RequestMapping(value="/test/list",  method = {RequestMethod.POST, RequestMethod.GET})
    public EmpInfo getTestList() throws Exception {
        System.out.println("getEmpInfoDetail test !!! ");
        EmpInfo empInfo = new EmpInfo();
        int empNo = 7839;
        empInfo = empInfoService.getEmpInfoDetail(empNo);
        return empInfo;
    }
}
