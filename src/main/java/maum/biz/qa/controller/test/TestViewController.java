package maum.biz.qa.controller.test;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class TestViewController {

    @RequestMapping(value="/test/testView", method = RequestMethod.GET)
    public String goCsSttList(){ return "common/test/testView"; }
}
