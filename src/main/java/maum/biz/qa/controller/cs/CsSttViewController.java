package maum.biz.qa.controller.cs;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CsSttViewController {

    // CS - STT 대상 목록
    @RequestMapping(value="/cs/csStt", method = RequestMethod.GET)
    public String goCsSttList(){ return "cs/csStt"; }
}
