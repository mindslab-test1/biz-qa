package maum.biz.qa.controller.common;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 메인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class HomeController {

    private static final Logger logger = LoggerFactory.getLogger(HomeController.class);

    @GetMapping("/home")
    public ModelAndView home(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("HomeController.{}", "home()");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "dashboardHome");
        httpSession.setAttribute("menuName", "DASHBOARD");
        httpSession.setAttribute("subMenuName", "");

        ModelAndView modelAndView = new ModelAndView();

        modelAndView.setViewName("common/home/dashboard");

        Gson gson = new Gson();

        logger.info("home() httpSession : {}", gson.toJson(httpSession));

        return modelAndView;
    }

}