package maum.biz.qa.controller.common;

import com.google.gson.Gson;
import maum.biz.qa.common.utils.seed.SeedCryptUtil;
import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.model.common.entity.UserPrincipal;
import maum.biz.qa.service.common.AdminUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * 로그인 컨트롤러
 *
 * @author unongko
 * @version 1.0
 */

@Controller
public class LoginController {

    @Autowired
    private AdminUserService adminUserService;

    @Value("${env.seedkey}")
    String seedkey;

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @GetMapping(value = {"/", "login"})
    public String getLoginPage(HttpServletRequest request, Model model) {
        logger.info("LoginController.{}", "getLoginPage()");

        model.addAttribute("errormsg",request.getParameter("errormsg"));
        return "noLayout/common/login/login";
    }

    @GetMapping(value = {"/", "accessDenied"})
    public String getAccessDeniedPage(Model model) {
        logger.info("LoginController.{}", "getAccessDeniedPage()");

        return "noLayout/common/login/login";
    }

    @RequestMapping(value = "/loginGate")
    public String getLoginGate(@RequestParam(value="empno", required = false) String empno, HttpServletRequest request, HttpServletResponse response, Model model) throws Exception {
        logger.info("getLoginGate.{}", "getLoginGate()");
        Gson gson = new Gson();

        logger.info("getLoginGate. {} ", empno);
//        String key = "#sTt45tA!Sim8pLq";
        String decParamVal = SeedCryptUtil.decryptDataHex(empno, seedkey);

        logger.info("Decoding empno      :: [" + decParamVal + "]");

        // 사번 날짜시간 구하기
        int idx = decParamVal.indexOf("_");
        String decEmpno = decParamVal.substring(0,idx);
        String strDate = decParamVal.substring(idx+1);
        logger.info("LoginController getLoginGate userId : {} ", decEmpno);
        logger.info("LoginController getLoginGate strDate : {} ", strDate);

        // 유효시간 체크

        // 사용자아이디조회
        String loginId = adminUserService.getLoginId(decEmpno);
        logger.info("LoginController loginId : {} " ,loginId);

        UserPrincipal userPrincipal = new UserPrincipal();

        userPrincipal = (UserPrincipal) adminUserService.loadUserByUsername("admin");
        logger.info("user.getAuthorities()  : {}", userPrincipal.getAuthorities());


        // 로그인 유지시간 체크


        // 유효한 사번 로그인 처리
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(userPrincipal.getId(), "", userPrincipal.getAuthorities()));

        AdminUser adminUser = new AdminUser();
        try{
            adminUser = adminUserService.getLoginAdminUserDetail(loginId);
        }catch (Exception e){
            e.printStackTrace();
        }

        logger.info("LoginController user : {} " ,gson.toJson(adminUser));

        if(adminUser != null){
            HttpSession session = request.getSession();
            session.setAttribute("accessUser", adminUser);
        }

        String redirectUri = "redirect:/home";

        return redirectUri;
    }

}