package maum.biz.qa.service.cs;


import maum.biz.qa.model.common.entity.EmpInfo;
import org.springframework.stereotype.Service;

@Service
public interface EmpInfoService {

    public EmpInfo getEmpInfoDetail(Integer empNo) throws Exception;
}

