package maum.biz.qa.service.cs;

import maum.biz.qa.mapper.oracle.cs.EmpInfoMapper;
import maum.biz.qa.model.common.entity.EmpInfo;
import org.mariadb.jdbc.internal.logging.Logger;
import org.mariadb.jdbc.internal.logging.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpInfoServiceImpl implements EmpInfoService {

    private static final Logger logger = LoggerFactory.getLogger(EmpInfoService.class);

    @Autowired
    private EmpInfoMapper empInfoMapper;

    public EmpInfo getEmpInfoDetail(Integer empNo) throws Exception {

        EmpInfo empInfo = new EmpInfo();

        empInfo = empInfoMapper.getEmpInfoDetail(empNo);

        logger.info("EmpInfoService empInfo :::::: "+ empInfo);

        return empInfo;
    }


}
