package maum.biz.qa.service.common;

import com.google.gson.Gson;
import maum.biz.qa.mapper.mariadb.common.AdminUserMapper;
import maum.biz.qa.model.common.dto.AdminUserDto;
import maum.biz.qa.model.common.form.AdminUserForm;
import maum.biz.qa.model.common.dto.CommonDto;
import maum.biz.qa.model.common.form.CommonForm;
import maum.biz.qa.model.common.dto.ResultDto;
import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.model.common.entity.UserPrincipal;
import maum.biz.qa.common.utils.PagingUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;

/**
 * 관리사용자 비지니스로직 서비스
 *
 * @author unongko
 * @version 1.0
 */

@Service
public class AdminUserService implements UserDetailsService {

	private static final Logger logger = LoggerFactory.getLogger(AdminUserService.class);

	@Autowired
	private AdminUserMapper adminUserMapper;

	public UserPrincipal findUserByLoginId(String loginId) {
		return adminUserMapper.findUserByLoginId(loginId);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserPrincipal userPrincipal = adminUserMapper.findUserByLoginId(username);
		return userPrincipal;
	}

	public String getLoginId(String decEmpno) throws Exception{
		return adminUserMapper.getLoginId(decEmpno);
	}

	public AdminUser getLoginAdminUserDetail(String userId) throws Exception {

		AdminUser adminUser = new AdminUser();

		adminUser = adminUserMapper.getLoginAdminUserDetail(userId);

		return adminUser;
	}

}
