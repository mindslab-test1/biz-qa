package maum.biz.qa.service.common;

import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.model.common.entity.UserPrincipal;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.LockedException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 스프링시큐리티 사용자 권한 및 인증 처리를 위한 Provider
 *
 * @author unongko
 * @version 1.0
 */


//@Component("authProvider")
public class UserAuthenticationProvider extends DaoAuthenticationProvider {

    private static final Logger logger = LoggerFactory.getLogger(UserAuthenticationProvider.class);
    
    @Autowired
    private AdminUserService adminUserService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {

        AdminUser adminUser = new AdminUser();
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        logger.info("UserAuthenticationProvider() username : {} " , username);
//        logger.info("UserAuthenticationProvider() password : {} " , password);

        UserPrincipal userPrincipal = new UserPrincipal();

        if(!"".equals(username) && username != null){
            userPrincipal = (UserPrincipal) adminUserService.loadUserByUsername(username);
            if(userPrincipal == null){
                throw new UsernameNotFoundException(username);
            }
        }else{
            throw new UsernameNotFoundException(username);
        }


        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

        adminUser.setUserId(username);

        //비밀번호 확인
        if(!passwordEncoder.matches(password, userPrincipal.getPassword())) {
            //계정상태 확인
            if(!userPrincipal.isEnabled()) {
                logger.info("UserAuthenticationProvider() LockedException ");
                throw new LockedException(username);
            }else{
                throw new BadCredentialsException(username);
            }
        }

        //계정상태 확인
        if(!userPrincipal.isEnabled()) {
            logger.info("UserAuthenticationProvider() LockedException");
            throw new LockedException(username);
        }

        return new UsernamePasswordAuthenticationToken(username, password, userPrincipal.getAuthorities());
    }

    @Override
    public boolean supports(Class<?> authentication) {
        return true;
    }
 
}