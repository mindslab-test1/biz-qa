package maum.biz.qa.common.utils;


public class StringUtil {

    // HTML element, value binding 제거
    public String removeElement(String text){
        if (null != text && !text.equals("")){
            // HTML element 제거 (ex: <b v=true> </b>)
            text = text.replaceAll("<(/)?([a-zA-Z0-9]*)(\\s[a-zA-Z0-9]*=[^>]*)?(\\s)*(/)?>", "");
            // 변수 바인딩 기호 제거 (ex: <&variable1&>
            text = text.replaceAll("<&([a-zA-Z0-9*+/-]*)(\\s[a-zA-Z0-9*+/-]*=[^>]*)?(\\s)*&>", "000");
        }
        return text;
    }

}
