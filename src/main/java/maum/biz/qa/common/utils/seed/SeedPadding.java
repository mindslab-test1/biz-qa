package maum.biz.qa.common.utils.seed;

/**
 * [암호화 블럭 Padding]
 * @author 	Choi Woo Keun
 * @version 	2009.02.17
 *              2009.05.11 롯데홈쇼핑을 위한 I/F에서 튜닝후 조정
 *                         패키지명을 통합하기 위해 com.hyundaicard.bics.crypto.seed 에서 변경
 *                         패딩 적용방법 바이트 배열 예로서 설명 추가
 *                         BISLAB H.PALMAN
 *   			개요		 : 암호화에서 블럭 사이즈를 맞추기 위해 사용되는 Padding
 * 				Padding 규칙 : [ANSI X.923] 사용
 */
public class SeedPadding {
    static private final byte PADDING_VALUE = 0x00;
    static private final String PADDING_NAME = "ANSI-X.923-Padding";

    /**
     * [Padding 추가]
     * @param  blkSrc       - byte[] : 패딩을 추가할 대상
     * @param  nBlockSize   - int    : Block size
     * @return              - byte[] : 패딩이 추가된 결과
     * @see                 - setPadding
     */
    static public byte[] insertPadding(byte[] blkSrc, int nBlockSize) {
        int nRems = blkSrc.length % nBlockSize;

        if (nRems != 0) {
            byte[] blkAddResult = new byte[blkSrc.length + (nBlockSize - nRems)];

            System.arraycopy(blkSrc, 0, blkAddResult, 0, blkSrc.length);

            setPadding(blkAddResult, blkSrc.length, nBlockSize);

            return blkAddResult;
        } else {
            return blkSrc;
        }
    }

    /**
     * 지정한 블럭에 패딩을 적용한다.
     * @param blkBuf 반드시 nBlockSize 여야 함.
     * @param srcFilledLen blkBuf에 채워진 길이
     * @param nBlockSize   blkBuf의 버퍼 사이즈로서 SeedBlockSize여야 한다.
     * @return 패딩한 길이를 반환함
     * 예) I: 0A 0B AA BB CC DD EE .. 이고, srcFilledLen이 7일 때
     *     O: 0A 0B AA BB CC DD EE 00 00 00 00 00 00 00 00 09
     *     를 blkBuf에 설정하며, 9 를 반환한다.
     * 예) I: 41 42 AA BB CC DD EE 32 33 34 35 36 37 38 39 이고, srcFilledLen이 15일 때
     *     O: 41 42 AA BB CC DD EE 32 33 34 35 36 37 38 39 01 을
     *     를 blkBuf에 설정하며, 1 을 반환한다.
     */
    static public int setPadding(byte[] blkBuf, int srcFilledLen,
                                 int nBlockSize) {

        if ((srcFilledLen % nBlockSize) <= 0)
            return 0;

        //int nAddLen = nBlockSize - srcFilledLen;
        int nAddLen = nBlockSize - (srcFilledLen % nBlockSize);

        for (int i = 0; i < (nAddLen - 1); i++) {
            blkBuf[srcFilledLen + i] = PADDING_VALUE;
        }

        blkBuf[srcFilledLen + nAddLen - 1] = (byte) nAddLen;
        return nAddLen;
    }

    /**
     * 지정한 블럭으로부터 패딩을 제거할 길이를 반환한다.
     * @param blkBuf       반드시 SeedBlockSize여야 하며, 역시 nBlockSize여야 함.
     * @param srcFilledLen 의미없음.
     * @param nBlockSize   SeedBlockSize
     * @return             제거한 패딩의 길이를 반환
     * 예) I: 0A 0B AA BB CC DD EE 00 00 00 00 00 00 00 00 09
     *     일때 패딩의 길이는 9를 반환한다.
     */
    static public int delPadding(byte[] blkBuf, int srcFilledLen,
                                 int nBlockSize) {
        int nAddLen = blkBuf[blkBuf.length - 1]; // 마지막 값이 패딩한 길이임.
        if (nAddLen <= 0 || nAddLen >= nBlockSize)
            return 0;

        for (int i = 0; i < (nAddLen - 1); i++) {
            if (blkBuf[(blkBuf.length - nAddLen) + i] != PADDING_VALUE) {
                return 0;
            }
        }
        return nAddLen;
    }

    /**
     * Padding 제거한 후의 새로운 또는 기존의 버퍼를 반환함.
     * @param blkSrc     - byte[]	: 패딩을 제거할 대상
     * @param nBlockSize - int		: Block size
     * @return           - byte[]	: 패딩이 제거된 결과를 반환
     */
    static public byte[] removePadding(byte[] blkSrc, int nBlockSize) {
        int nPadLen = blkSrc[blkSrc.length - 1];

        // ex1) 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 01 --> 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45
        // ex2) 31 32 33 34 35 36 37 38 39 40 41 42 43 44 00 02 --> 31 32 33 34 35 36 37 38 39 40 41 42 43 44
        // ex3) 31 00 00 00 00 00 00 00 00 00 00 00 00 00 00 15 --> 31

        if (nPadLen > 0 && nPadLen < nBlockSize) {
            for (int i = 0; i < (nPadLen - 1); i++) {
                if (blkSrc[(blkSrc.length - nPadLen) + i] != PADDING_VALUE) {
                    return blkSrc; // 원본 그대로 반환
                }
            }

            byte[] blkRet = new byte[blkSrc.length - nPadLen];

            System.arraycopy(blkSrc, 0, blkRet, 0, blkRet.length);

            return blkRet;
        }
        return blkSrc; // 원본 그대로 반환
    }

    public String getName() {
        return PADDING_NAME;
    }
}