package maum.biz.qa.common.handler;

import com.google.gson.Gson;
import maum.biz.qa.model.common.entity.AdminUser;
import maum.biz.qa.service.common.AdminUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * 스프링시큐리티 로그인 성공 처리 핸들러
 *
 * @author unongko
 * @version 1.0
 */

@Component
public class LoginSuccessHandler implements AuthenticationSuccessHandler {

	private static final Logger logger = LoggerFactory.getLogger(LoginSuccessHandler.class);

	@Autowired
	private AdminUserService adminUserService;
	
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		Gson gson = new Gson();
		logger.info("LoginSuccessHandler auth : {} ", gson.toJson(auth));

		String userId = auth.getName();
		logger.info("LoginSuccessHandler session userId : {} ", userId);

		AdminUser adminUser = new AdminUser();
		try{
			adminUser = adminUserService.getLoginAdminUserDetail(userId);
		}catch (Exception e){
			e.printStackTrace();
		}

		logger.info("LoginSuccessHandler user : {} " ,gson.toJson(adminUser));

		if(adminUser != null){
			HttpSession session = request.getSession();
			session.setAttribute("accessUser", adminUser);
		}		
		
		response.sendRedirect(request.getContextPath() + "/home");
	}
}