package maum.biz.qa.common.config;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@MapperScan(value="maum.biz.qa.mapper.oracle", sqlSessionFactoryRef="sqlSessionFactory2")
public class OracleDataSourceConfig {
    @Autowired
    private Environment env;

    private final String MYBATIS_CONFIG = "classpath:mybatis/mybatis-config.xml";
    private final String MYBATIS_MAPPER = "classpath:mybatis/mapper/oracle/**/*.xml";



    @Bean
    @ConfigurationProperties(prefix = "spring.oracle.datasource")
    public DataSource oracleDataSource() {

        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.oracle.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.oracle.datasource.url"));
        dataSource.setUsername(env.getProperty("spring.oracle.datasource.username"));
        dataSource.setPassword(env.getProperty("spring.oracle.datasource.password"));
        return dataSource;
    }

    @Bean
    public SqlSessionFactory sqlSessionFactory2(@Autowired @Qualifier("oracleDataSource") DataSource dataSource) throws Exception {
        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
        factoryBean.setDataSource(dataSource);
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();

        factoryBean.setConfigLocation(resolver.getResource(MYBATIS_CONFIG));
        factoryBean.setMapperLocations(resolver.getResources(MYBATIS_MAPPER));

        return factoryBean.getObject();
    }

    @Bean
    public SqlSession sqlSession2(@Autowired @Qualifier("sqlSessionFactory2") SqlSessionFactory factory) {
        return new SqlSessionTemplate(factory);
    }
}









