//package maum.biz.qa.common.config;
//
//import org.apache.ibatis.session.SqlSession;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.mybatis.spring.annotation.MapperScan;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Primary;
//import org.springframework.core.env.Environment;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.jdbc.datasource.DriverManagerDataSource;
//
//import javax.sql.DataSource;
//
//@Configuration
//@MapperScan(value="maum.biz.qa.mapper.mysql", sqlSessionFactoryRef="sqlSessionFactory3")
//public class MysqlDataSourceConfig {
//
//    @Autowired
//    private Environment env;
//
//    private final String MYBATIS_CONFIG = "classpath:mybatis/mybatis-config.xml";
//    private final String MYBATIS_MAPPER = "classpath:mybatis/mapper/mysql/**/*.xml";
//
//    @Bean
//    @ConfigurationProperties(prefix = "spring.mysql.datasource")
//    public DataSource mysqlDataSource() {
//        DriverManagerDataSource dataSource = new DriverManagerDataSource();
//        dataSource.setDriverClassName(env.getProperty("spring.mysql.datasource.driver-class-name"));
//        dataSource.setUrl(env.getProperty("spring.mysql.datasource.url"));
//        dataSource.setUsername(env.getProperty("spring.mysql.datasource.username"));
//        dataSource.setPassword(env.getProperty("spring.mysql.datasource.password"));
//
//        return dataSource;
//    }
//
//
//    @Bean
//    public SqlSessionFactory sqlSessionFactory3(@Autowired @Qualifier("mysqlDataSource") DataSource dataSource) throws Exception {
//        SqlSessionFactoryBean factoryBean = new SqlSessionFactoryBean();
//        factoryBean.setDataSource(dataSource);
//        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//        factoryBean.setConfigLocation(resolver.getResource(MYBATIS_CONFIG));
//        factoryBean.setMapperLocations(resolver.getResources(MYBATIS_MAPPER));
//
//        return factoryBean.getObject();
//    }
//
//    @Bean
//    public SqlSession sqlSession3(@Autowired @Qualifier("sqlSessionFactory3") SqlSessionFactory factory) {
//        return new SqlSessionTemplate(factory);
//    }
//}
//
//
//
//
//
//
//
//
