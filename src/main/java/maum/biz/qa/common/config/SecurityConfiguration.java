package maum.biz.qa.common.config;

import maum.biz.qa.common.handler.LoginFailureHandler;
import maum.biz.qa.common.handler.LoginSuccessHandler;
import maum.biz.qa.service.common.AdminUserService;
import maum.biz.qa.service.common.UserAuthenticationProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
/**
 * 스프링부트 스프링시큐리티 Config
 *
 * @author unongko
 * @version 1.0
 */

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    private AdminUserService adminUserService;

    @Autowired
    LoginSuccessHandler loginSuccessHandler;

    @Autowired
    LoginFailureHandler loginFailureHandler;

    @Bean
    public UserAuthenticationProvider authenticationProvider(AdminUserService adminUserService) {
        UserAuthenticationProvider authenticationProvider = new UserAuthenticationProvider();
        authenticationProvider.setUserDetailsService(adminUserService);
        authenticationProvider.setPasswordEncoder(bCryptPasswordEncoder);
        return authenticationProvider;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider(adminUserService));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().sameOrigin();
        http
            .authorizeRequests()
                .antMatchers("/login").permitAll()
                .antMatchers("/test").permitAll()
                .antMatchers("/loginGate").permitAll()
                .antMatchers("/test/**").permitAll()
                .antMatchers("/admin/**").hasAuthority("ROLE_ADMIN")
            .anyRequest()
                .authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .loginProcessingUrl("/loginProcess")
                .failureUrl("/login?error=true")
                .usernameParameter("userId")
                .passwordParameter("password")
                .successHandler(loginSuccessHandler)
                .failureHandler(loginFailureHandler)
            .and()
                .logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
                .invalidateHttpSession(true)
            .and()
                .exceptionHandling()
                .accessDeniedPage("/accessDenied")
        ;
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring()
                .antMatchers("/resources/css/**")
                .antMatchers("/resources/js/**")
                .antMatchers("/resources/font/**")
                .antMatchers("/resources/images/**")
                .antMatchers("/favicon**")
        ;
    }

}