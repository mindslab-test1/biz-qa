package maum.biz.qa.common.config;

import lombok.Getter;
import lombok.Setter;
import maum.biz.qa.model.common.entity.PropertyItemVo;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import javax.annotation.PostConstruct;

/**
 * 스프링부트 프로퍼티 Config
 *
 * @author unongko
 * @version 1.0
 */

@Getter
@Setter
@Configuration
@EnableConfigurationProperties
@PropertySource(value="classpath:application-${spring.profiles.active}.yml")
@ConfigurationProperties(prefix="env")
public class PropertyConfig {
    private String servers;
    private PropertyItemVo obj;
}
