package maum.biz.qa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MaumQaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaumQaApplication.class, args);
    }

}

